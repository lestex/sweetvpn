class PublicController < ApplicationController
	#layout 'landing'
	layout :resolve_layout

  def index
    if user_signed_in?
      redirect_to (private_path)
    else
      render "index"
    end
  end

  def prices

  end

  def faq

  end

  def support

  end

  def terms

  end

  def about
    
  end
  def settings
    
  end

  def resolve_layout
    case action_name
    when "index"
      "landing"    
    else
      "application"
    end
  end
end
